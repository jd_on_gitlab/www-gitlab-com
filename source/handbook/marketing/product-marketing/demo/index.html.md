---
layout: markdown_page
title: "Demos"
---

# On this page
{:.no_toc}

- TOC
{:toc}

# About Demos

There are 3 basic demos types:
* Vision - designed to show where we are going (this might include forward looking ideas not yet developed)
* Use Case Based - designed to show how GitLab solves a particular use case (how we should be selling)
* Feature - designed to enable a deeper dive into details of product features (on prospect request)

These demos come in different formats:
* videos - good for everyone to self-demo by passively watching
* click-throughs (screenshots on slides) - good for disconnected situations (like conferences) and for interactive self demos
* live (instructions for how to set up and run demos live) - enables everyone (especially GitLab sales) to set up and run through the demo live on their own system resources. Allows for spontaneous deep diving - not always a good thing).

# Videos

## GitLab Overview - Planning to Monitoring (11.3) (Oct 2018)
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/nMAgP4WIcno" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

## 2018 Vision Prototype (Feb 2018)
<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/RmSTLGnEmpQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

## Auto DevOps in GitLab 11.0 (June 2018)

<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/0Tc0YYBxqi4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

## GitLab VSM - Issue Boards for Mapping (June 2018)

<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/9ASHiQ2juYY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

## GitLab VSM - Business Value Monitoring (June 2018)

<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/oG0VESUOFAI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

## GitLab for the Enterprise (Feb 2018)

<figure class="video_container">
 <iframe width="560" height="315" src="https://www.youtube.com/embed/gcWfUw_Cau4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

# Click-throughs

Click-through demos can be run in multiple ways:
* [Run them off-line](off-line-click-throughs/) (for conferences, practice on the airplane, etc)
* Run them while online, [from the files](https://drive.google.com/drive/folders/1Qm8Y3oVLRa0nS1BARA631Ex6SKVzYp3C?usp=sharing), same as running a presentation
* Or run them online from the web, using the links bellow:

## Auto DevOps - Run (Oct 2018 - 11.3)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTNeBj0TJWca1PVwccxUTXWDWYqaIyB6Q1fRYCfjtMzeK8DtpmAcG1o6ipFBi-lhYKVTAA9kYBWyKKu/embed?start=false&loop=false&delayms=600000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
[G-slides file](https://docs.google.com/presentation/d/1oKHU3MsbJmxVQyO-7c6JLMoCOS80uS-0NlcI-mRxAAY/edit?usp=sharing)

## Auto DevOps - Setup (Oct 2018 - 11.3)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQIPrinsvTG1s6ppnUWqSY-fpHnxe6oAwM7g91uBl8Mx3EYQhaejlKUF9_c3GtagIhzwg-8dJlIrNgw/embed?start=false&loop=false&delayms=600000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
[G-slides file](https://docs.google.com/presentation/d/1AGABPlNzMm5-rrYfwGIzueXIbPleVkGpnc2Qk6JtnWk/edit?usp=sharing)

## Auto DevOps (in 3 minutes) (Juy 2018 - 11.0)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRQ4xltHhYRO_zgbo7exF6BwR09jvPmyFzR4XvjdlpYMRqT4dctx61XCkLjfR-8sq6QyOsoEFBBJjJh/embed?start=false&loop=false&delayms=600000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
[G-slides file](https://docs.google.com/presentation/d/1UkQI_9V-CJZcbZJBDTB7tyOg14XHCKIwNoUHW1K6tC8/edit?usp=sharing)

## Security Capabilities (May 2018 - 11.0)

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQ6mgFN_gj8q3hOY6_vyStbqkcKAN1FCEaxUYdopH2HwqnweU12Jol35mFqh8vyHXVDFEisVgomrcFs/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
[G-slides file](https://docs.google.com/presentation/d/1Ipnrlz0-DnjZ9UaOI4djo3GRoLlt_l2nNw89pWfaCTg/edit?usp=sharing)

# Live (instructions)

## [Planning to Monitoring (formerly i2p)](i2p/)
Highlights GitLab’s single platform for the complete DevOps lifecycle, from idea to production, through issues, planning, merge request, CI/CD, and monitoring.

## [CI/CD Deep Dive](cicd-deep/)
Provides a more in-depth look at GitLab CI/CD pipelines.

## [Integration Demos](integrations/)
Demonstrations which highlight integrations between GitLab and common tools such as Jira issues and Jenkins pipelines.
