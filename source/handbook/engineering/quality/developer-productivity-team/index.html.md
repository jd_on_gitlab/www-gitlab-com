---
layout: markdown_page
title: "Developer productivity team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Engineers in this team are dedicated to our [Developer productivity](/handbook/engineering/quality#developer-productivity) efforts.

## Areas of Responsibility

* Make metrics-driven suggestions to improve engineering processes, velocity, and throughput.
  * Build automated [measurements and dashboards] to gain insights into
    developer productivity and understand what is working and what is not.
    * Make suggestions for improvements, monitor the results and iterate.
    * See the [GitLab Insights] project.
  * Ensure workflow and label hygiene in our system which feeds into our metrics dashboard.
    * Automated issues and merge requests triage.
    * Automated triage package generation.
    * See the [GitLab Triage] and [GitLab triage operations] projects.

* Build productivity tooling to help speed up development efforts.
  * [Automated CE->EE merge].
  * Increase contributor and developer productivity by improving the development setup, workflow, processes, and tools.
    * Improve the ease of use of our [GDK (GitLab Development Kit)].
    * Improve [Review apps] for CE/EE (GDK in the cloud).

* Build automated tools to ensure the consistency and quality of the codebase and merge request workflow.
  * Automated [merge request coaching](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/Dangerfile).
  * Automated [merge request code quality checks](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/.rubocop.yml).

### Members

* [Rémy Coutable](/team/#rymai): General expert.
* [Lin Jen-shin](/team/#godfat): CE / EE architecture, and CI expert.
* [Mark Fletcher](/team/#markglenfletcher): Issue triage expert.

[measurements and dashboards]: http://quality-dashboard.gitlap.com/groups/gitlab-org
[GitLab Insights]: https://gitlab.com/gitlab-org/gitlab-insights
[GitLab Triage]: https://gitlab.com/gitlab-org/gitlab-triage
[GitLab triage operations]: https://gitlab.com/gitlab-org/quality/triage-ops
[Automatic CE->EE merge]: https://docs.gitlab.com/ee/development/automatic_ce_ee_merge.html
[GDK (GitLab Development Kit)]: https://gitlab.com/gitlab-org/gitlab-development-kit
[Review apps]: https://docs.gitlab.com/ee/development/testing_guide/review_apps.html
