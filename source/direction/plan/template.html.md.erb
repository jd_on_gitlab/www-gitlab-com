---
layout: markdown_page
title: "Product Vision - Plan"
---

- TOC
{:toc}

## Overview 

The [GitLab DevOps lifecycle](/direction/#devops-stages) contains the Plan stage.
The product vision of Plan is to enable all people in any size organization to:

- **Innovate** ideas.
- Organize those ideas into transparent, **multi-level work breakdown plans** that cut
across departments.
- **Track the execution** of these plans, adjusting them along the way as needed.
- **Collaborate** with team members, internal and external stakeholders, and executive
sponsors, using the same set of single-source-of-truth artifacts in a single application.
- **Measure** the efficacy of the entire process, characterizing the flows with 
**value streams**, thus providing actionable insight for continuous improvement.

## Detailed vision

- [Timeline-based roadmap view of upcoming planned improvements](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=Plan&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS).
- [Upcoming planned strategic direction improvements](https://gitlab.com/groups/gitlab-org/-/boards/706864?label_name[]=direction) 
and [upcoming planned improvements in detail](https://gitlab.com/groups/gitlab-org/-/boards/706864)
for the next few milestones (monthly releases)

### Agile portfolio and project management (PPM)

Large enterprises are continually working on increasingly more complex and larger
scope initiatives that cut across multiple teams and even departments, spanning months,
quarters, and even years. GitLab's vision is to allow organizing these initiatives 
into powerful **multi-level work breakdown** plans and enable **tracking the execution** 
of them over time, to be extremely simple and insightful. In addition, GitLab should 
highlight which **opportunities have higher ROI**, helping teams make strategic 
business planning decisions.

GitLab's **multi-level work breakdown** planning capabilities will include [multiple layers of epics](https://gitlab.com/groups/gitlab-org/-/epics/312) 
and [issues](https://gitlab.com/groups/gitlab-org/-/epics/316), allowing enterprises to capture:

- High-level strategic initiatives and OKRs (objectives and key results).
- Portfolios, programs, and projects.
- Product features and other improvements.
- [Work roll ups](https://gitlab.com/groups/gitlab-org/-/epics/312) and [dependency management](https://gitlab.com/groups/gitlab-org/-/epics/316).

GitLab will leverage [timeline-based roadmap visualizations](https://gitlab.com/gitlab-org/gitlab-ee/issues/7077)
to help enterprises plan from small time scales (e.g. 2 week sprints for development 
teams) to large time scales (e.g. annual strategic initiatives for entire departments).
Enhanced issues and [issue boards](https://gitlab.com/groups/gitlab-org/-/epics/293) 
[capabilities](https://gitlab.com/groups/gitlab-org/-/epics/383) will allow more 
versatile and powerful backlog grooming and cross-sprint planning functionality.

GitLab will enable teams to **track execution** of these plans over time. In addition
to roadmap views as well as existing time tracking and story point estimation features 
(called weights in GitLab), GitLab workflow management will also be improved with 
[group-level customized workflows integrated into boards](https://gitlab.com/groups/gitlab-org/-/epics/424),
as well as [burndown and burnup charts also integrated into boards](https://gitlab.com/groups/gitlab-org/-/epics/372).

GitLab will help teams **identify [high ROI](https://gitlab.com/gitlab-org/gitlab-ee/issues/7718) [opportunities](https://gitlab.com/gitlab-org/gitlab-ee/issues/8115)**,
by organizing and surfacing initiatives with relatively large benefits (as entered
by team members) and relatively low costs (via rolled up effort estimates at the 
epic level).

GitLab supports [enterprise Agile portfolio and project management frameworks](https://about.gitlab.com/solutions/agile-delivery/), 
including Scaled Agile Framework (SAFe), Scrum, and Kanban.

See how GitLab team members themselves use GitLab Agile PPM to create GitLab, as
of GitLab 11.4.

<figure class="video_container">
<iframe src="https://www.youtube.com/embed/ME9VwseXMuo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

See [upcoming planned improvements for Agile portfolio and project management](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=Agile).

### Governed workflows

Large enterprises, especially those in highly-regulated industries such as finance
and healthcare, rely on governance frameworks in their planning and execution, in
order to mitigate a variety of business risks.

GitLab will be improved with [customized workflows integrated into boards](https://gitlab.com/groups/gitlab-org/-/epics/424), 
supporting the specific governed workflow stages in a particular organization. Also,
GitLab will have [custom fields](https://gitlab.com/groups/gitlab-org/-/epics/235), 
enabling the level of oversight and standardization required in the given large company.

See [upcoming planned improvements for governed workflows](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=governance).

### Value streams and analytics

The enterprise software industry is quickly transitioning to an Agile DevOps delivery
model, where the focus is no longer only on getting bits and bytes out the door,
but instead on shipping iterative business value continuously to end users, with 
rapid customer feedback.

GitLab will be enhanced with value streams and analytics built natively into the 
application itself. [Customized workflows](https://gitlab.com/groups/gitlab-org/-/epics/424) 
will allow teams to define their own value streams, relevant to their own company, 
their own customers, and their own industry. GitLab will surface [VSM (value stream management) style metrics](https://gitlab.com/groups/gitlab-org/-/epics/229),
such as cycle time, lead time, and deployment frequency, aggregated and rolled up, 
per an individual team, across multiple teams within a department, or even across 
the entire organization. Since GitLab is the first single application for the entire 
DevOps lifecycle, [these metrics](https://gitlab.com/groups/gitlab-org/-/epics/313) 
will allow teams to quickly identify and pinpoint process gaps and areas needing 
urgent improvement.

See [upcoming planned improvements for value streams and analytics](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=analytics).

### Search

Teams leverage GitLab search to quickly search for relevant content, enabling
stronger intra-team and cross-team collaboration through discovery of all GitLab
data.

See [upcoming planned improvements for search](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=search).

### Jira integration

GitLab supports deep Jira integration, allowing teams to use Jira for issue
mangement, but still leveraging the benefits of GitLab source control and other
native GitLab features.

See [upcoming planned improvements for Jira integration](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=Jira).

### Requirements and quality management

- [Requirements management](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=✓&state=opened&label_name[]=requirements%20management)
- [Quality management](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=✓&state=opened&label_name[]=quality%20management)

## How we prioritize

We follow the [general prioritization process](/handbook/product/#prioritization)
of the Product Team. In particular, we consider reach, impact, confidence, and
effort to identify and plan changes in upcoming milestones (monthly iterations).

- See a high-level [timeline-based roadmap view of planned upcoming improvements](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=Plan&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS).
- See [upcoming planned strategic direction improvements](https://gitlab.com/groups/gitlab-org/-/boards/706864?label_name[]=direction) 
and [upcoming planned improvements in detail](https://gitlab.com/groups/gitlab-org/-/boards/706864)
for the next few milestones (monthly releases).

## Contributions and feedback

We love community code contributions to GitLab. Read
[this guide](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/index.md)
to get started.

Please also participate in our [issue tracker](https://gitlab.com/gitlab-org/gitlab-ce).
You can file bugs, propose new features, suggest design improvements, or
continue a conversation on any one of these. Simply [open a new issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/new)
or comment on [an existing one](https://gitlab.com/gitlab-org/gitlab-ce/issues).

You can also contact me (Victor) via the channels on [my profile page](https://gitlab.com/victorwu).