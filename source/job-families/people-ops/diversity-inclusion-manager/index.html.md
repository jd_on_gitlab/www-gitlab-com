---
layout: job_page
title: "Diversity and Inclusion Manager"
---

The Diversity and Inclusion Manager will help build an inclusive environment where all employees feel equally welcomed and are able to bring their best selves to work every day. As GitLab continues to grow, we look for people from a wide range of backgrounds to reflect the global nature of our customers. It takes diversity of thought, culture, background, and perspective to build a truly global company. This role will work closely with recruiting to help shape a recruiting strategy that provides diverse candidate pipelines to fuel GitLab’s growth.

## Responsibilities

- Partner with the People Operations team, organizational leaders, and People Business Partners to continue building out our diversity and inclusion strategy
- Consult with organizational leaders to provide mentorship and coaching on how to achieve results
- Works with the Recruiting team to create and implement a diversity recruitment strategy
- Develop and improve relationships with current and new diversity organizations and partnerships
- Increase organizational capabilities in cross-cultural intelligence, mitigating unconscious bias, and inclusive leadership
- Provide communication and visibility of D&I efforts across GitLab
- Design leadership development programs focused on underrepresented groups
- Review and analyze data to monitor progress
- Conduct research and benchmarking on D&I standard methodologies and emerging trends and apply that to relevant programs aligned to business needs
- Collaborate strategically to engage employees, amplify successes, and ensure inclusive internal and external messaging
- Build out a library of diversity and inclusion materials

## Requirements

- 5+ years of experience in human resources or related business experience
- 3+ years of experience leading diversity projects, diversity sourcing initiatives, and/or recruiting initiatives
- 3+ years leadership experience, preferably in the D&I space
- Strong business acumen and ability to connect D&I learning to business needs
- Deep understanding of diversity and inclusion principles and practices
- Ability to influence leadership and enroll employee support and engagement
- Skilled at taking a consultative and collaborative approach to problem solving
- Experience driving change management initiatives successfully and flexing with shifting priorities
- Experienced facilitation and communication skills
- Strong project management and analytical skills
- Proven passion for inclusion and diversity
- Excellent EQ and an empathetic communication style that fosters connection, collaboration, and confidence
- Strong ability to plan proactively and react quickly when problems arise
- Confidence to rely on own judgment and discretion to work independently, keep multiple plates spinning, and take smart risks
- Experience working in a global environment preferred
- You share our [values](/handbook/values), and work in accordance with those values
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks)

## Hiring Process

- Qualified candidates will be invited to schedule a 30 minute screening call with one of our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute panel interview with one of our Senior Recruiters and our Diversity Sourcer
- Next, candidates will be invited to schedule a 45 minute interview with our Recruiting Director
- After that, candidates will be invited to schedule a 30 minute interview with our Chief Culture Officer
- Next, the candidate will be invited to interview with a People Business Partner
- After that, the candidate will be invited to interview with our CFO or another member of our executive team
- Finally, our CEO may choose to conduct a final interview
