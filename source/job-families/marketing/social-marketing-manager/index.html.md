---
layout: job_page
title: "Social Marketing Manager"
---

The world’s best brands feel more like a friend to their audience than a company. This is because the world’s best brands understand people, and weave that understanding into everything they do. The Social Marketing Manager is a key part of building GitLab’s brand into one that is loved by its audience of developers, IT ops practitioners, and IT leaders.

This role will focus on understanding people who make up the GitLab community, including those who use GitLab (or are potential users), sharing content over social channels that they’ll find useful and valuable, and helping the rest of the marketing department understand the people that make up the markets that GitLab is serving.

## Associate Responsibilities

- Own strategic social media planning in support of product launches, company news, crisis communications, and brand campaigns by determining what to publish, when, and where.
- Work with the team to ensure social amplification and engagement is incorporated as part of integrated campaign strategy and execution
- Be the voice of our consumers to our internal teams. Advocate for their needs, represent their feedback, and flag opportunities to do more, and identify potential pitfalls
- Grow GitLab’s social fan base while ensuring the quality of our followers and the relationships we've built with them on Twitter, LinkedIn, Instagram, Facebook, and Google+
- Identify key social influencers and work to develop and cultivate relationship and engagement.
- Ensure day-to-day publishing, reporting, and strategies are in place
- Manage the social calendar and craft social campaigns that will help engage, grow and educate our followers
- Social listening: Monitor conversations about GitLab online to flag positive brand opportunities that can be converted into meaningful marketing materials via offline activities and user-generated content (blog posts, speaking opps, interviews, etc.)
- Assist Community Advocates in flagging negative conversations and brand associations
- Handle reporting for social media initiatives, including channel health, campaign, and launch reports

## Associate Requirements

- In-depth knowledge and understanding of social media platforms and their respective uses including Twitter, Facebook, LinkedIn, YouTube, and Google+
- Excellent writing and communication skills
- Highly collaborative. This role will require effective collaboration across multiple teams, organizations, and individuals.
- Extremely detail-oriented and organized
- Ability to confidently write and engage in the GitLab brand voice and personality in real time
- You share our [values](/handbook/values/), and work in accordance with those values
- BONUS: A passion and strong understanding of the industry and our mission

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team/).

- Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule a series of 45 minute interviews with our Content Marketing Manager, Content Editor, and Community Advocate
- Candidates will then be invited to schedule 45-minute interviews with our Senior Director of Marketing and Sales Development and our CMO.
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
